#include "cache.h"

void init(Handle<Object> /*exports*/, Handle<Object> module)
{
    Cache::init(module);
}

NODE_MODULE(cache, init)
