# Fast cache for NodeJS #

**NOTE:** This is a very early version. If you have found a bug or you have a suggestion on improvement, please feel free to create an issue about it.

How is this cache better than the (so many) others you ask? That is simple! The other modules are written in pure javascript, while this one is done completely in C++, taking advantage of the **unordered_map** container, combined with the fast **xxHash** algorithm.

## Installation ##

```
#!js

npm install node-fast-cache

```

## Example usage ##

```
#!js

var Cache = require("node-fast-cache");
var cache = new Cache();

cache.set("foo", "bar");
cache.get("foo");
```

## Cache Reference ##

### Constructor properties ###

The constructor accepts **options** object with the following keys:

- **ttl** - time to live value to be used, if it is not specified when using **set**. Default is *0*.
- **cleanInterval** - how often the cache should be checked for expired values. Default is *120* (in seconds).
- **cleanMaxElements** - how many elements (at maximum) should be removed, if expired, when the clean up occurs. 

### Simple methods ###

- **set** - function (key, value, *optional* ttl)  
Add a new element to the cache, mapping it to the specified **key**. It overwrites an existing value with that key. The **ttl**(timeToLive) specifies how much time (in **seconds**), the value should be kept in the cache.

- **get** - function (key)  
Retrieves an element. Returns **undefined**, if the element was not found or the value.

- **del** - function (key)  
Deletes an element, while also returning its value, if the element was found.

- **clear** - function ()  
Invalidates the cache (deletes everything in it).

### Multi-data methods ###

**NOTE**: All methods in this section return an array that contains the results of the operation.

- **mset** - function (array)  
Accepts an array of objects. Each object must have **key** and **value**, while **ttl** is optional.

- **mget** - function (array)  
Accepts an array of strings.

- **mdel** - function (array)  
Accepts an array of strings.

# LICENSE #
The MIT License (MIT)

Copyright (c) 2014 Atanas Plachkov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.