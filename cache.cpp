#include "cache.h"

void ttlTimeoutCallback(uv_timer_t *handle, int status);

void Cache::init(Handle<Object> module)
{
    Local<FunctionTemplate> cacheJS = FunctionTemplate::New(Constructor);
    cacheJS->SetClassName(String::NewSymbol("Cache"));
    cacheJS->InstanceTemplate()->SetInternalFieldCount(6);

    // Prototype
    Local<ObjectTemplate> proto = cacheJS->PrototypeTemplate();

    proto->Set(String::NewSymbol("get"), FunctionTemplate::New(get)->GetFunction());
    proto->Set(String::NewSymbol("set"), FunctionTemplate::New(set)->GetFunction());
    proto->Set(String::NewSymbol("del"), FunctionTemplate::New(remove)->GetFunction());
    proto->Set(String::NewSymbol("clear"), FunctionTemplate::New(removeAll)->GetFunction());

    proto->Set(String::NewSymbol("mget"), FunctionTemplate::New(getMultiple)->GetFunction());
    proto->Set(String::NewSymbol("mset"), FunctionTemplate::New(setMultiple)->GetFunction());
    proto->Set(String::NewSymbol("mdel"), FunctionTemplate::New(removeMultiple)->GetFunction());

    module->Set(String::NewSymbol("exports"), Persistent<Function>::New(cacheJS->GetFunction()));
}

Cache::Cache(uint ttl, uint cleanInterval, uint cleanMaxElements)
{
    mTTL = ttl;
    mCleanInterval = cleanInterval;
    mCleanMaxElements = cleanMaxElements;
    mCleanerHandle.data = 0;
}

Cache::~Cache()
{
    clear();
}

Handle<Value> Cache::Constructor(const Arguments &args)
{
    Cache *cache;

    uint cTtl;
    uint cCleanInterval;
    uint cCleanMaxElements;

    if (args.Length() > 0)
    {
        // We assume that first argument is object (it should be...)
        if (args[0]->IsObject())
        {
            Local<Object> obj = Local<Object>::Cast(args[0]);
            Local<Value> ttl = obj->Get(String::New("ttl"));
            Local<Value> cleanInterval = obj->Get(String::New("cleanInterval"));
            Local<Value> cleanMaxElements = obj->Get(String::New("cleanMaxElements"));

            cTtl = ttl->IsUndefined() ? DEFAULT_TTL : ttl->Uint32Value();
            // in milliseconds
            cCleanInterval = (cleanInterval->IsUndefined() ? DEFAULT_CLEAN_INTERVAL : cleanInterval->Uint32Value()) * 1000;
            cCleanMaxElements = cleanMaxElements->IsUndefined() ? DEFAULT_CLEAN_MAX_ELEMENTS : cleanInterval->Uint32Value();
        }
        else
        {
            return ThrowException(Exception::TypeError(String::New("The constructor's first element should be an options object or nothing!")));
        }
    }
    else
    {
        cTtl = DEFAULT_TTL;
        cCleanInterval = DEFAULT_CLEAN_INTERVAL;
        cCleanMaxElements = DEFAULT_CLEAN_MAX_ELEMENTS;
    }

    cache = new Cache(cTtl, cCleanInterval, cCleanMaxElements);

    Local<Object> self = args.This();
    cache->Wrap(self);

    // Init cleaning timer
    uv_loop_t *loop = uv_default_loop();
    uv_timer_init(loop, &cache->mCleanerHandle);

    // Start cleaning
    cache->mCleanerHandle.data = cache;
    uv_timer_start(&cache->mCleanerHandle, ttlTimeoutCallback, cCleanInterval, cCleanInterval);

    // Unref the timer, so that it will NOT prevent the app from shutting down
    uv_unref((uv_handle_t *)&cache->mCleanerHandle);

    return self;
}

Handle<Value> Cache::get(const Arguments &args)
{
    HandleScope scope;

    if (args.Length() != 1)
    {
        return Undefined();
    }

    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());
    citerator cit = cache->mStore.find(Persistent<Value>(args[0]));

    return scope.Close(cache->get(cit));
}

Handle<Value> Cache::set(const Arguments &args)
{
    HandleScope scope;
    int alen = args.Length();

    if (alen < 2 || alen > 3)
    {
        return ThrowException(Exception::TypeError(String::NewSymbol("Invalid number of arguments.")));
    }

    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());
    Persistent<Value> key = Persistent<Value>::New(args[0]);
    Persistent<Value> value = Persistent<Value>::New(args[1]);

    return cache->set(key, value, alen == 3 ? args[2]->Uint32Value() : cache->mTTL);
}

Handle<Value> Cache::remove(const Arguments &args)
{
    HandleScope scope;

    if (args.Length() < 1)
    {
        return Undefined();
    }

    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());
    citerator cit = cache->mStore.find(Persistent<Value>(args[0]));

    return scope.Close(cache->remove(cit));
}

Handle<Value> Cache::removeAll(const Arguments &args)
{
    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());
    if (cache->mStore.size() > 0)
    {
        cache->clear();
        return True();
    }
    return False();
}

Handle<Value> Cache::getMultiple(const Arguments &args)
{
    HandleScope scope;

    if (args.Length() != 1)
    {
        return Undefined();
    }

    if (!args[0]->IsArray())
    {
        return ThrowException(Exception::TypeError(String::New("The passed in argument should be an array.")));
    }

    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());

    Local<Array> arr = Local<Array>::Cast(args[0]);
    uint arrLen = arr->Length();

    Local<Array> result = Array::New(arrLen);
    if (result.IsEmpty())
    {
        // If the result array could not be created return "null"
        return Null();
    }

    for (uint i = 0; i < arrLen; i++)
    {
        citerator cit = cache->mStore.find(Persistent<Value>(arr->Get(i)));
        result->Set(i, cache->get(cit));
    }

    return scope.Close(result);
}

Handle<Value> Cache::setMultiple(const Arguments &args)
{
    HandleScope scope;

    if (args.Length() != 1)
    {
        return Undefined();
    }

    if (!args[0]->IsArray())
    {
        return ThrowException(Exception::TypeError(String::New("The passed in argument should be an array.")));
    }

    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());

    Local<Array> arr = Local<Array>::Cast(args[0]);
    uint arrLen = arr->Length();

    Local<Array> result = Array::New(arrLen);
    if (result.IsEmpty())
    {
        // If the result array could not be created return "null"
        return Null();
    }

    for (uint i = 0; i < arrLen; i++)
    {
        Local<Object> el = arr->Get(i).As<Object>();
        if (!el->IsObject())
        {
            return ThrowException(Exception::TypeError(String::New("Each element should be an object with at least \"key\" and \"value\" properties.")));
        }

        Persistent<Value> key = Persistent<Value>::New(el->Get(String::New("key")));
        Persistent<Value> value = Persistent<Value>::New(el->Get(String::New("value")));
        Local<Value> ttl = el->Get(String::New("ttl"));

        result->Set(i, cache->set(key, value, ttl->IsUndefined() ? cache->mTTL : ttl->Uint32Value()));
    }

    return scope.Close(result);
}

Handle<Value> Cache::removeMultiple(const Arguments &args)
{
    HandleScope scope;

    if (args.Length() != 1)
    {
        return Undefined();
    }

    if (!args[0]->IsArray())
    {
        return ThrowException(Exception::TypeError(String::New("The passed in argument should be an array.")));
    }

    Cache *cache = ObjectWrap::Unwrap<Cache>(args.This());

    Local<Array> arr = Local<Array>::Cast(args[0]);
    uint arrLen = arr->Length();

    Local<Array> result = Array::New(arrLen);
    if (result.IsEmpty())
    {
        // If the result array could not be created return "null"
        return Null();
    }

    for (uint i = 0; i < arrLen; i++)
    {
        citerator cit = cache->mStore.find(Persistent<Value>(arr->Get(i)));
        result->Set(i, cache->remove(cit));
    }

    return scope.Close(result);
}

void Cache::clear()
{
    for (citerator it = mStore.begin(); it != mStore.end(); )
    {
        Persistent<Value> value = it->first;
        value.Dispose();
        value.Clear();

        value = it->second->value;
        value.Dispose();
        value.Clear();

        delete it->second;

        it = mStore.erase(it);
    }

    mExpirationOrder.clear();
}

Handle<Value> Cache::get(citerator cit)
{
    if (cit == mStore.end())
    {
        return Undefined();
    }

    uint time = (uint) std::time(0);
    StoreData *sd = cit->second;

    // Check if it is not expired
    if (sd->ttl && time > sd->lastTimeUsed + sd->ttl)
    {
        remove(cit, false);
        return Undefined();
    }
    sd->lastTimeUsed = time;

    if (sd->ttl)
    {
        // It's now the most recently used
        mExpirationOrder.erase(sd->lit);
        mExpirationOrder.push_front(sd);
        sd->lit = mExpirationOrder.begin();
    }

    return sd->value;
}

Handle<Value> Cache::set(Persistent<Value> key, Persistent<Value> value, uint ttl)
{
    citerator cit = mStore.find(key);

    if (cit != mStore.end())
    {
        remove(cit, false);
    }

    StoreData *sd = new StoreData;
    sd->value = value;
    sd->ttl = ttl;
    sd->lastTimeUsed = (uint) std::time(0);

    // Only put them in for future cleaning if there is some ttl set
    if (ttl)
    {
        mExpirationOrder.push_front(sd);
        sd->lit = mExpirationOrder.begin();
    }

    std::pair<citerator, bool> pair = mStore.insert(std::pair<Persistent<Value>, StoreData *>(key, sd));
    sd->cit = pair.first;

    return True();
}

Handle<Value> Cache::remove(citerator cit, bool checkIfValid)
{
    if (checkIfValid && cit == mStore.end())
    {
        return Undefined();
    }

    Local<Value> val = Local<Value>::New(cit->second->value);

    Persistent<Value> oldVal = cit->second->value;
    oldVal.Dispose();
    oldVal.Clear();
    StoreData *sd = cit->second;

    if (sd->ttl)
    {
        mExpirationOrder.erase(sd->lit);
    }
    delete sd;

    oldVal = cit->first;
    oldVal.Dispose();
    oldVal.Clear();

    mStore.erase(cit);

    return val;
}

void ttlTimeoutCallback(uv_timer_t *handle, int status)
{
    Cache *cache = static_cast<Cache *>(handle->data);
    list<StoreData *> *exp = &cache->mExpirationOrder;
    StoreData *sd;

    uint cleanedUp = 0;
    uint maxClean = cache->mCleanMaxElements;
    uint time = (uint) std::time(0);

    // No elements == we are done
    if (exp->size() == 0)
    {
        return;
    }

    // Reverse loop since the LRU item is at the end
    literator it = exp->end(); --it;
    do
    {
        sd = *it;
        if (sd->ttl)
        {
            if (time > sd->lastTimeUsed + sd->ttl)
            {
                // Clear the KEY's memory in the store
                Persistent<Value> val = sd->cit->first;
                val.Dispose();
                val.Clear();

                // Erase the pair from the store
                cache->mStore.erase(sd->cit);

                // Remove from the expiration order
                it = exp->erase(it);

                // Delete the data that is carried
                val = sd->value;
                val.Dispose();
                val.Clear();
                delete sd;

                if (++cleanedUp == maxClean)
                {
                    // Stop executing for now (don't take too much CPU time)
                    // TODO: Add event to finish up, thus not blocking execution with this only
                    return;
                }
            }
            else
            {
                // If we reach an element that still has not expired => the rest elements are OK!
                break;
            }
        }
        else
        {
            --it;
        }
    } while (it != exp->begin());
}
