#ifndef CACHE_H
#define CACHE_H

#include <ctime>
#include "v8_hash.h"

// Unlimited TTL (except if the objects don't become too many)
#define DEFAULT_TTL 0

// 2 minutes (in seconds)
#define DEFAULT_CLEAN_INTERVAL 120

// Max deletions per clean up
#define DEFAULT_CLEAN_MAX_ELEMENTS 1000

typedef unsigned int uint;

struct StoreData;

typedef list<StoreData *>::iterator literator;
typedef unordered_map<Persistent<Value>, StoreData *, v8_key_hash, v8_key_equality> CacheStore;
typedef CacheStore::iterator citerator;

struct StoreData {
    Persistent<Value> value;
    uint ttl;
    uint lastTimeUsed;
    literator lit;
    citerator cit;
};

class Cache : public node::ObjectWrap
{
    friend void ttlTimeoutCallback(uv_timer_t *handle, int status);
public:
    static void init(Handle<Object> module);

    Handle<Value> get(citerator cit);
    Handle<Value> set(Persistent<Value> key, Persistent<Value> value, uint ttl);
    Handle<Value> remove(citerator cit, bool checkIfValid = true);
    void clear();

private:
    explicit Cache(uint, uint, uint);
    ~Cache();

    static Handle<Value> Constructor(const Arguments &args);

    // Simple
    static Handle<Value> get(const Arguments &args);
    static Handle<Value> set(const Arguments &args);
    static Handle<Value> remove(const Arguments &args);     // a.k.a "del"
    static Handle<Value> removeAll(const Arguments &args);  // a.k.a "clear"

    // Multiple
    static Handle<Value> getMultiple(const Arguments &args);
    static Handle<Value> setMultiple(const Arguments &args);
    static Handle<Value> removeMultiple(const Arguments &args);     // a.k.a "del"

    // Counters

    // Vars
    CacheStore mStore;
    uint mTTL;
    uint mCleanInterval;
    uint mCleanMaxElements;
    list<StoreData *> mExpirationOrder;

    uv_timer_t mCleanerHandle;
};

#endif // CACHE_H
